<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A88795">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Verses spoken to the King, Queen, and Dutchesse of York in St John's Library in Oxford</title>
    <author>Lawrence, Thomas, 1645?-1714.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A88795 of text R202709 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing L623B). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 5 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A88795</idno>
    <idno type="STC">Wing L623B</idno>
    <idno type="STC">ESTC R202709</idno>
    <idno type="EEBO-CITATION">99899595</idno>
    <idno type="PROQUEST">99899595</idno>
    <idno type="VID">154247</idno>
    <idno type="PROQUESTGOID">2248513816</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. A88795)</note>
    <note>Transcribed from: (Early English Books Online ; image set 154247)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2407:10)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Verses spoken to the King, Queen, and Dutchesse of York in St John's Library in Oxford</title>
      <author>Lawrence, Thomas, 1645?-1714.</author>
     </titleStmt>
     <extent>[2], 2 p.</extent>
     <publicationStmt>
      <publisher>printed by H. Hall, for R. Davis,</publisher>
      <pubPlace>[Oxford :</pubPlace>
      <date>1663]</date>
     </publicationStmt>
     <notesStmt>
      <note>Sometimes attributed to Thomas Laurence.</note>
      <note>Imprint from Wing (CD-ROM edition).</note>
      <note>Signatures: *⁴.</note>
      <note>"The title (without any imprint) and the sign. * on the title and *2 on p.1, show that this piece was intended to go with [Sir Thomas Ireland's Verses spoken at the appearance of the King and Queene (Wing I296)]: the second leaf is sometimes found as a separate piece"--Madan.</note>
      <note>Bound with and identified as part of Wing I296 on UMI microfilm "Early English books, 1641-1700" reel 458.</note>
      <note>Reproduction of original in the Folger Shakespeare Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Charles II, 1660-1685 -- Poetry -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Verses spoken to the King, Queen, and Dutchesse of York in St John's Library in Oxford.</ep:title>
    <ep:author>Lawrence, Thomas, </ep:author>
    <ep:publicationYear>1663</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>10</ep:pageCount>
    <ep:wordCount>367</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2009-09</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2009-09</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-10</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-10</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2010-04</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A88795-e10010">
  <front xml:id="A88795-e10020">
   <div type="title_page" xml:id="A88795-e10030">
    <pb facs="tcp:154247:1" xml:id="A88795-001-a"/>
    <pb facs="tcp:154247:1" xml:id="A88795-001-b"/>
    <pb facs="tcp:154247:2" xml:id="A88795-002-a"/>
    <pb facs="tcp:154247:2" xml:id="A88795-002-b"/>
    <p xml:id="A88795-e10040">
     <w lemma="verse" pos="n2" rend="hi" xml:id="A88795-002-b-0010">VERSES</w>
     <w lemma="speak" pos="vvn" xml:id="A88795-002-b-0020">Spoken</w>
     <w lemma="to" pos="acp" xml:id="A88795-002-b-0030">to</w>
     <w lemma="the" pos="d" xml:id="A88795-002-b-0040">the</w>
     <w lemma="king" pos="n1" xml:id="A88795-002-b-0050">KING</w>
     <pc xml:id="A88795-002-b-0060">,</pc>
     <w lemma="QVEEN" pos="nn1" xml:id="A88795-002-b-0070">QVEEN</w>
     <pc xml:id="A88795-002-b-0080">,</pc>
     <w lemma="and" pos="cc" xml:id="A88795-002-b-0090">and</w>
     <w lemma="duchess" pos="n1" reg="DUCHESS" rend="hi" xml:id="A88795-002-b-0100">DVTCHESSE</w>
     <w lemma="of" pos="acp" xml:id="A88795-002-b-0110">of</w>
     <w lemma="York" pos="nn1" reg="YORK" xml:id="A88795-002-b-0120">YORKE</w>
     <w lemma="in" pos="acp" xml:id="A88795-002-b-0130">in</w>
     <w lemma="st" orig="Sᵗ" pos="ab" xml:id="A88795-002-b-0140">St</w>
     <w lemma="JOHN" pos="nng1" xml:id="A88795-002-b-0160">JOHN'S</w>
     <w lemma="library" pos="n1" xml:id="A88795-002-b-0170">Library</w>
     <w lemma="in" pos="acp" xml:id="A88795-002-b-0180">in</w>
     <w lemma="Oxford" pos="nn1" rend="hi" xml:id="A88795-002-b-0190">Oxford</w>
     <pc unit="sentence" xml:id="A88795-002-b-0200">.</pc>
    </p>
   </div>
  </front>
  <body xml:id="A88795-e10090">
   <div type="poem" xml:id="A88795-e10100">
    <lg xml:id="A88795-e10110">
     <pb facs="tcp:154247:3" xml:id="A88795-003-a"/>
     <pb facs="tcp:154247:3" n="1" xml:id="A88795-003-b"/>
     <head xml:id="A88795-e10120">
      <hi xml:id="A88795-e10130">
       <w lemma="speak" pos="vvn" xml:id="A88795-003-b-0010">Spoken</w>
       <w lemma="to" pos="acp" xml:id="A88795-003-b-0020">to</w>
       <w lemma="the" pos="d" xml:id="A88795-003-b-0030">the</w>
      </hi>
      <w lemma="king" pos="n1" xml:id="A88795-003-b-0040">KING</w>
      <w lemma="and" pos="cc" rend="hi" xml:id="A88795-003-b-0050">and</w>
      <w lemma="queen" pos="n1" xml:id="A88795-003-b-0060">QUEEN</w>
      <w lemma="in" pos="acp" rend="hi" xml:id="A88795-003-b-0070">in</w>
      <w lemma="st" orig="Sᵗ" pos="ab" xml:id="A88795-003-b-0080">St</w>
      <w lemma="John" pos="nng1" xml:id="A88795-003-b-0100">John's</w>
      <w lemma="library" pos="n1" xml:id="A88795-003-b-0110">Library</w>
      <pc unit="sentence" xml:id="A88795-003-b-0120">.</pc>
     </head>
     <l xml:id="A88795-e10170">
      <w lemma="bird" pos="ng1" reg="Bird's" xml:id="A88795-003-b-0130">BIrds</w>
      <w lemma="have" pos="vvb" xml:id="A88795-003-b-0140">have</w>
      <w lemma="find" pos="vvn" xml:id="A88795-003-b-0150">found</w>
      <w lemma="language" pos="n1" xml:id="A88795-003-b-0160">Language</w>
      <pc xml:id="A88795-003-b-0170">,</pc>
      <w lemma="elephant" pos="n2" xml:id="A88795-003-b-0180">Elephants</w>
      <w lemma="a" pos="d" xml:id="A88795-003-b-0190">a</w>
      <w lemma="knee" pos="n1" xml:id="A88795-003-b-0200">Knee</w>
     </l>
     <l xml:id="A88795-e10180">
      <w lemma="to" pos="prt" xml:id="A88795-003-b-0210">To</w>
      <w lemma="compliment" pos="vvi" reg="Compliment" xml:id="A88795-003-b-0220">Complement</w>
      <w lemma="the" pos="d" xml:id="A88795-003-b-0230">the</w>
      <w lemma="approach" pos="n1" xml:id="A88795-003-b-0240">approach</w>
      <w lemma="of" pos="acp" xml:id="A88795-003-b-0250">of</w>
      <w lemma="majesty" pos="n1" xml:id="A88795-003-b-0260">Majesty</w>
      <pc xml:id="A88795-003-b-0270">:</pc>
     </l>
     <l xml:id="A88795-e10190">
      <w lemma="none" pos="pix" xml:id="A88795-003-b-0280">None</w>
      <w lemma="so" pos="av" xml:id="A88795-003-b-0290">so</w>
      <w lemma="much" pos="av-d" xml:id="A88795-003-b-0300">much</w>
      <w lemma="statue" pos="n1" xml:id="A88795-003-b-0310">statue</w>
      <w lemma="but" pos="acp" xml:id="A88795-003-b-0320">but</w>
      <pc join="right" xml:id="A88795-003-b-0330">(</pc>
      <w lemma="like" pos="acp" xml:id="A88795-003-b-0340">like</w>
      <w lemma="Memnon" pos="nng1" reg="Memnon's" rend="hi" xml:id="A88795-003-b-0350">Memnons</w>
      <pc xml:id="A88795-003-b-0360">)</pc>
      <w lemma="play" pos="vvz" reg="plays" xml:id="A88795-003-b-0370">playes</w>
     </l>
     <l xml:id="A88795-e10210">
      <w lemma="anthem" pos="n2" xml:id="A88795-003-b-0380">Anthems</w>
      <w lemma="to" pos="prt" xml:id="A88795-003-b-0390">to</w>
      <w lemma="welcome" pos="vvi" xml:id="A88795-003-b-0400">welcome</w>
      <w lemma="such" pos="d" xml:id="A88795-003-b-0410">such</w>
      <w lemma="illustrious" pos="j" xml:id="A88795-003-b-0420">illustrious</w>
      <w lemma="ray" pos="n2" reg="rays" xml:id="A88795-003-b-0430">rayes</w>
      <pc unit="sentence" xml:id="A88795-003-b-0440">.</pc>
     </l>
     <l xml:id="A88795-e10220">
      <w lemma="your" pos="po" xml:id="A88795-003-b-0450">Your</w>
      <w lemma="presence" pos="n1" xml:id="A88795-003-b-0460">presence</w>
      <pc xml:id="A88795-003-b-0470">,</pc>
      <w lemma="madam" pos="n1" rend="hi" xml:id="A88795-003-b-0480">Madam</w>
      <pc xml:id="A88795-003-b-0490">,</pc>
      <w lemma="here" pos="av" xml:id="A88795-003-b-0500">here</w>
      <w lemma="do" pos="vvz" xml:id="A88795-003-b-0510">doth</w>
      <w lemma="parallel" pos="vvi" reg="parallel" xml:id="A88795-003-b-0520">paralel</w>
     </l>
     <l xml:id="A88795-e10240">
      <w lemma="our" pos="po" xml:id="A88795-003-b-0530">Our</w>
      <w lemma="baptist" pos="nn2" xml:id="A88795-003-b-0540">Baptists</w>
      <w lemma="desert" pos="n1" xml:id="A88795-003-b-0550">desert</w>
      <w lemma="to" pos="acp" xml:id="A88795-003-b-0560">to</w>
      <w lemma="a" pos="d" xml:id="A88795-003-b-0570">a</w>
      <w lemma="boscobel" pos="nn1" rend="hi" xml:id="A88795-003-b-0580">Boscobel</w>
      <pc unit="sentence" xml:id="A88795-003-b-0590">.</pc>
     </l>
     <l xml:id="A88795-e10260">
      <w lemma="our" pos="po" xml:id="A88795-003-b-0600">Our</w>
      <w lemma="mother" pos="n1" xml:id="A88795-003-b-0610">Mother</w>
      <w lemma="glory" pos="n2" xml:id="A88795-003-b-0620">glories</w>
      <w lemma="that" pos="cs" xml:id="A88795-003-b-0630">that</w>
      <w lemma="your" pos="po" xml:id="A88795-003-b-0640">your</w>
      <w lemma="smile" pos="n2" xml:id="A88795-003-b-0650">smiles</w>
      <w lemma="upon" pos="acp" xml:id="A88795-003-b-0660">upon</w>
      <w lemma="she" pos="pno" xml:id="A88795-003-b-0670">her</w>
     </l>
     <l xml:id="A88795-e10270">
      <w lemma="create" pos="vvi" xml:id="A88795-003-b-0680">Create</w>
      <w lemma="her" pos="po" xml:id="A88795-003-b-0690">her</w>
      <w lemma="virgin" pos="n1" xml:id="A88795-003-b-0700">Virgin</w>
      <w lemma="muse" pos="n2" xml:id="A88795-003-b-0710">Muses</w>
      <w lemma="maid" pos="n2" xml:id="A88795-003-b-0720">maids</w>
      <w lemma="of" pos="acp" xml:id="A88795-003-b-0730">of</w>
      <w lemma="honour" pos="n1" xml:id="A88795-003-b-0740">Honour</w>
      <pc unit="sentence" xml:id="A88795-003-b-0750">.</pc>
     </l>
     <l xml:id="A88795-e10280">
      <w lemma="your" pos="po" xml:id="A88795-003-b-0760">Your</w>
      <w lemma="station" pos="n1" xml:id="A88795-003-b-0770">station</w>
      <w lemma="betwixt" pos="acp" xml:id="A88795-003-b-0780">'twixt</w>
      <w lemma="these" pos="d" xml:id="A88795-003-b-0790">these</w>
      <w lemma="globe" pos="n2" xml:id="A88795-003-b-0800">Globes</w>
      <w lemma="do" pos="vvz" xml:id="A88795-003-b-0810">doth</w>
      <w lemma="prompt" pos="vvi" xml:id="A88795-003-b-0820">prompt</w>
      <w lemma="our" pos="po" xml:id="A88795-003-b-0830">our</w>
      <w lemma="pen" pos="n1" xml:id="A88795-003-b-0840">pen</w>
     </l>
     <l xml:id="A88795-e10290">
      <w lemma="to" pos="acp" xml:id="A88795-003-b-0850">To</w>
      <w lemma="fancy" pos="n1" reg="fancy" xml:id="A88795-003-b-0860">fansie</w>
      <w lemma="prince" pos="n2" xml:id="A88795-003-b-0870">Princes</w>
      <w lemma="place" pos="vvn" reg="placed" xml:id="A88795-003-b-0880">plac'd</w>
      <w lemma="betwixt" pos="acp" xml:id="A88795-003-b-0890">'twixt</w>
      <w lemma="god" pos="n2" xml:id="A88795-003-b-0900">Gods</w>
      <w lemma="and" pos="cc" xml:id="A88795-003-b-0910">and</w>
      <w lemma="man" pos="n2" xml:id="A88795-003-b-0920">men</w>
      <pc xml:id="A88795-003-b-0930">;</pc>
     </l>
     <l xml:id="A88795-e10300">
      <w lemma="here" pos="av" xml:id="A88795-003-b-0940">Here</w>
      <w lemma="man" pos="n2" xml:id="A88795-003-b-0950">men</w>
      <pc xml:id="A88795-003-b-0960">,</pc>
      <w lemma="there" pos="av" xml:id="A88795-003-b-0970">there</w>
      <w lemma="angel" pos="n2" xml:id="A88795-003-b-0980">Angels</w>
      <w lemma="ply" pos="vvb" xml:id="A88795-003-b-0990">ply</w>
      <w lemma="their" pos="po" xml:id="A88795-003-b-1000">their</w>
      <w lemma="different" pos="j" xml:id="A88795-003-b-1010">different</w>
      <w lemma="sphere" pos="n2" xml:id="A88795-003-b-1020">Spheres</w>
      <pc xml:id="A88795-003-b-1030">,</pc>
     </l>
     <l xml:id="A88795-e10310">
      <w lemma="our" pos="po" xml:id="A88795-003-b-1040">Our</w>
      <w lemma="house" pos="n1" xml:id="A88795-003-b-1050">house</w>
      <w lemma="of" pos="acp" xml:id="A88795-003-b-1060">of</w>
      <w lemma="commons" pos="n2" xml:id="A88795-003-b-1070">Commons</w>
      <pc xml:id="A88795-003-b-1080">,</pc>
      <w lemma="and" pos="cc" xml:id="A88795-003-b-1090">and</w>
      <w lemma="your" pos="po" xml:id="A88795-003-b-1100">your</w>
      <w lemma="house" pos="n1" xml:id="A88795-003-b-1110">House</w>
      <w lemma="of" pos="acp" xml:id="A88795-003-b-1120">of</w>
      <w lemma="peer" pos="n2" xml:id="A88795-003-b-1130">Peers</w>
      <pc unit="sentence" xml:id="A88795-003-b-1140">.</pc>
     </l>
     <l xml:id="A88795-e10320">
      <w lemma="may" pos="vmb" xml:id="A88795-003-b-1150">May</w>
      <w lemma="your" pos="po" xml:id="A88795-003-b-1160">your</w>
      <w lemma="last" pos="ord" xml:id="A88795-003-b-1170">last</w>
      <w lemma="progress" pos="n1" xml:id="A88795-003-b-1180">progress</w>
      <w lemma="here" pos="av" xml:id="A88795-003-b-1190">here</w>
      <w lemma="reach" pos="vvi" xml:id="A88795-003-b-1200">reach</w>
      <w lemma="Nestor" pos="nng1" rend="hi" xml:id="A88795-003-b-1210">Nestor's</w>
      <w lemma="sum" pos="n1" reg="Sum" xml:id="A88795-003-b-1220">Summe</w>
      <pc xml:id="A88795-003-b-1230">,</pc>
     </l>
     <l xml:id="A88795-e10340">
      <w lemma="till" pos="acp" xml:id="A88795-003-b-1240">Till</w>
      <w lemma="the" pos="d" xml:id="A88795-003-b-1250">the</w>
      <w lemma="supreme" pos="j" xml:id="A88795-003-b-1260">Supreme</w>
      <w lemma="star-chamber" pos="n1" xml:id="A88795-003-b-1270">Star-Chamber</w>
      <w lemma="call" pos="vvb" xml:id="A88795-003-b-1280">call</w>
      <w lemma="you" pos="pn" xml:id="A88795-003-b-1290">you</w>
      <w lemma="home" pos="av-n" xml:id="A88795-003-b-1300">home</w>
      <pc xml:id="A88795-003-b-1310">:</pc>
     </l>
     <l xml:id="A88795-e10350">
      <w lemma="while" pos="cs" reg="Whilst" xml:id="A88795-003-b-1320">Whil'st</w>
      <w lemma="angel" pos="n2" xml:id="A88795-003-b-1330">Angels</w>
      <w lemma="propagate" pos="vvi" xml:id="A88795-003-b-1340">propagate</w>
      <pc xml:id="A88795-003-b-1350">,</pc>
      <w lemma="and" pos="cc" xml:id="A88795-003-b-1360">and</w>
      <w lemma="you" pos="pn" xml:id="A88795-003-b-1370">you</w>
      <w lemma="display" pos="vvi" xml:id="A88795-003-b-1380">display</w>
     </l>
     <l xml:id="A88795-e10360">
      <w lemma="a" pos="d" xml:id="A88795-003-b-1390">A</w>
      <w lemma="little" pos="j" xml:id="A88795-003-b-1400">little</w>
      <w lemma="CHARLES" pos="nn1" xml:id="A88795-003-b-1410">CHARLES</w>
      <w lemma="his" pos="po" xml:id="A88795-003-b-1420">his</w>
      <w lemma="wain" pos="n1" reg="Wain" xml:id="A88795-003-b-1430">Waine</w>
      <pc xml:id="A88795-003-b-1440">,</pc>
      <w lemma="and" pos="cc" xml:id="A88795-003-b-1450">and</w>
      <w lemma="milky" pos="j" xml:id="A88795-003-b-1460">Milky</w>
      <w lemma="way" pos="n1" xml:id="A88795-003-b-1470">Way</w>
      <pc xml:id="A88795-003-b-1480">:</pc>
     </l>
     <l xml:id="A88795-e10370">
      <w lemma="these" pos="d" xml:id="A88795-003-b-1490">These</w>
      <w lemma="asterism" pos="n2" reg="Asterisms" xml:id="A88795-003-b-1500">Asterismes</w>
      <w lemma="be" pos="vvb" xml:id="A88795-003-b-1510">are</w>
      <w lemma="only" pos="av-j" xml:id="A88795-003-b-1520">only</w>
      <w lemma="want" pos="vvg" xml:id="A88795-003-b-1530">wanting</w>
      <w lemma="yet" pos="av" xml:id="A88795-003-b-1540">yet</w>
     </l>
     <l xml:id="A88795-e10380">
      <w lemma="to" pos="prt" xml:id="A88795-003-b-1550">To</w>
      <w lemma="make" pos="vvi" xml:id="A88795-003-b-1560">make</w>
      <hi xml:id="A88795-e10390">
       <w lemma="white" pos="j" reg="White" xml:id="A88795-003-b-1570">VVhite</w>
       <pc unit="sentence" xml:id="A88795-003-b-1580">.</pc>
       <w lemma="hall" pos="n1" xml:id="A88795-003-b-1590">Hall</w>
      </hi>
      <w lemma="a" pos="d" xml:id="A88795-003-b-1600">a</w>
      <w lemma="heaven" pos="n1" xml:id="A88795-003-b-1610">Heaven</w>
      <pc xml:id="A88795-003-b-1620">,</pc>
      <w lemma="and" pos="cc" xml:id="A88795-003-b-1630">and</w>
      <w lemma="heaven" pos="n1" xml:id="A88795-003-b-1640">Heaven</w>
      <w lemma="complete" pos="j" xml:id="A88795-003-b-1650">complete</w>
      <pc unit="sentence" xml:id="A88795-003-b-1660">.</pc>
     </l>
     <l xml:id="A88795-e10400">
      <w lemma="perfection" pos="n1" xml:id="A88795-003-b-1670">Perfection</w>
      <pc xml:id="A88795-003-b-1680">,</pc>
      <w lemma="madam" pos="n1" xml:id="A88795-003-b-1690">Madam</w>
      <pc xml:id="A88795-003-b-1700">,</pc>
      <w lemma="from" pos="acp" xml:id="A88795-003-b-1710">from</w>
      <w lemma="yourself" pos="pr" reg="yourself" xml:id="A88795-003-b-1720">your self</w>
      <w lemma="must" pos="vmb" xml:id="A88795-003-b-1740">must</w>
      <w lemma="grow" pos="vvi" xml:id="A88795-003-b-1750">grow</w>
      <pc xml:id="A88795-003-b-1760">:</pc>
     </l>
     <l xml:id="A88795-e10410">
      <w lemma="king" pos="n2" xml:id="A88795-003-b-1770">Kings</w>
      <w lemma="be" pos="vvb" xml:id="A88795-003-b-1780">are</w>
      <w lemma="immortal" pos="j" xml:id="A88795-003-b-1790">Immortal</w>
      <pc xml:id="A88795-003-b-1800">,</pc>
      <w lemma="but" pos="acp" xml:id="A88795-003-b-1810">but</w>
      <w lemma="queen" pos="n2" xml:id="A88795-003-b-1820">Queens</w>
      <w lemma="make" pos="vvb" xml:id="A88795-003-b-1830">make</w>
      <w lemma="they" pos="pno" xml:id="A88795-003-b-1840">them</w>
      <w lemma="so" pos="av" xml:id="A88795-003-b-1850">so</w>
      <pc unit="sentence" xml:id="A88795-003-b-1860">.</pc>
     </l>
    </lg>
   </div>
   <div type="poem" xml:id="A88795-e10420">
    <pb facs="tcp:154247:4" n="2" xml:id="A88795-004-a"/>
    <head xml:id="A88795-e10430">
     <hi xml:id="A88795-e10440">
      <w lemma="to" pos="prt" xml:id="A88795-004-a-0010">To</w>
      <w lemma="her" pos="po" xml:id="A88795-004-a-0020">her</w>
      <w lemma="highness" pos="n1" xml:id="A88795-004-a-0030">Highness</w>
      <w lemma="the" pos="d" xml:id="A88795-004-a-0040">the</w>
     </hi>
     <w lemma="duchess" pos="n1" reg="DUCHESS" xml:id="A88795-004-a-0050">DVTCHESSE</w>
     <hi xml:id="A88795-e10450">
      <w lemma="of" pos="acp" xml:id="A88795-004-a-0060">of</w>
      <w lemma="york" pos="nn1" xml:id="A88795-004-a-0070">YORK</w>
      <w lemma="in" pos="acp" xml:id="A88795-004-a-0080">in</w>
      <w lemma="the" pos="d" xml:id="A88795-004-a-0090">the</w>
      <w lemma="same" pos="d" xml:id="A88795-004-a-0100">same</w>
      <w lemma="place" pos="n1" xml:id="A88795-004-a-0110">place</w>
      <pc unit="sentence" xml:id="A88795-004-a-0120">.</pc>
     </hi>
    </head>
    <l xml:id="A88795-e10460">
     <w lemma="if" pos="cs" xml:id="A88795-004-a-0130">IF</w>
     <w lemma="duty" pos="n1" xml:id="A88795-004-a-0140">Duty</w>
     <w lemma="without" pos="acp" xml:id="A88795-004-a-0150">without</w>
     <w lemma="compliment" pos="n1" xml:id="A88795-004-a-0160">Compliment</w>
     <w lemma="may" pos="vmb" xml:id="A88795-004-a-0170">may</w>
     <w lemma="stand" pos="vvi" xml:id="A88795-004-a-0180">stand</w>
     <pc xml:id="A88795-004-a-0190">,</pc>
    </l>
    <l xml:id="A88795-e10470">
     <w lemma="and" pos="cc" xml:id="A88795-004-a-0200">And</w>
     <w lemma="they" pos="pns" xml:id="A88795-004-a-0210">they</w>
     <w lemma="who" pos="crq" xml:id="A88795-004-a-0220">who</w>
     <w lemma="can" pos="vmb" xml:id="A88795-004-a-0230">can</w>
     <w lemma="but" pos="acp" xml:id="A88795-004-a-0240">but</w>
     <w lemma="kneel" pos="vvi" xml:id="A88795-004-a-0250">kneel</w>
     <pc xml:id="A88795-004-a-0260">,</pc>
     <w lemma="may" pos="vmb" xml:id="A88795-004-a-0270">may</w>
     <w lemma="kiss" pos="vvi" xml:id="A88795-004-a-0280">kiss</w>
     <w lemma="your" pos="po" xml:id="A88795-004-a-0290">your</w>
     <w lemma="hand" pos="n1" xml:id="A88795-004-a-0300">Hand</w>
     <pc xml:id="A88795-004-a-0310">:</pc>
    </l>
    <l xml:id="A88795-e10480">
     <w lemma="if" pos="cs" xml:id="A88795-004-a-0320">If</w>
     <w lemma="muse" pos="ng1" reg="Muse's" xml:id="A88795-004-a-0330">Muses</w>
     <w lemma="country" pos="n1" xml:id="A88795-004-a-0340">Country</w>
     <w lemma="girl" pos="n2" reg="Girls" xml:id="A88795-004-a-0350">Girles</w>
     <w lemma="their" pos="po" xml:id="A88795-004-a-0360">their</w>
     <w lemma="skill" pos="n1" reg="skill" xml:id="A88795-004-a-0370">skil</w>
     <w lemma="may" pos="vmb" xml:id="A88795-004-a-0380">may</w>
     <w lemma="try" pos="vvi" xml:id="A88795-004-a-0390">try</w>
     <pc xml:id="A88795-004-a-0400">,</pc>
    </l>
    <l xml:id="A88795-e10490">
     <w join="right" lemma="though" pos="cs" xml:id="A88795-004-a-0410">Though</w>
     <w join="left" lemma="it" pos="pn" xml:id="A88795-004-a-0411">'t</w>
     <w lemma="spoil" pos="vvb" reg="spoil" xml:id="A88795-004-a-0420">spoile</w>
     <w lemma="a" pos="d" xml:id="A88795-004-a-0430">an</w>
     <w lemma="honour" pos="n1" xml:id="A88795-004-a-0440">Honour</w>
     <w lemma="to" pos="acp" xml:id="A88795-004-a-0450">to</w>
     <w lemma="a" pos="d" xml:id="A88795-004-a-0460">a</w>
     <w lemma="courtesy" pos="n1" reg="Courtesy" xml:id="A88795-004-a-0470">Courtesie</w>
     <pc xml:id="A88795-004-a-0480">:</pc>
    </l>
    <l xml:id="A88795-e10500">
     <w join="right" lemma="we" pos="pns" reg="we" xml:id="A88795-004-a-0490">Wee</w>
     <w join="left" lemma="will" pos="vmd" xml:id="A88795-004-a-0491">'d</w>
     <w lemma="rally" pos="vvi" xml:id="A88795-004-a-0500">rally</w>
     <w lemma="all" pos="d" xml:id="A88795-004-a-0510">all</w>
     <w lemma="our" pos="po" xml:id="A88795-004-a-0520">our</w>
     <w lemma="force" pos="n2" xml:id="A88795-004-a-0530">forces</w>
     <w lemma="to" pos="prt" xml:id="A88795-004-a-0540">to</w>
     <w lemma="express" pos="vvi" xml:id="A88795-004-a-0550">express</w>
    </l>
    <l xml:id="A88795-e10510">
     <w lemma="your" pos="po" xml:id="A88795-004-a-0560">Your</w>
     <w lemma="noble" pos="js" xml:id="A88795-004-a-0570">Noblest</w>
     <w lemma="welcome" pos="n1-j" xml:id="A88795-004-a-0580">Welcome</w>
     <w lemma="in" pos="acp" xml:id="A88795-004-a-0590">in</w>
     <w lemma="a" pos="d" xml:id="A88795-004-a-0600">a</w>
     <w lemma="plain" pos="j" xml:id="A88795-004-a-0610">plain</w>
     <w lemma="address" pos="n1" xml:id="A88795-004-a-0620">address</w>
     <pc xml:id="A88795-004-a-0630">:</pc>
    </l>
    <l xml:id="A88795-e10520">
     <w lemma="Mars" pos="nn1" rend="hi" xml:id="A88795-004-a-0640">Mars</w>
     <w join="right" lemma="we" pos="pns" reg="we" xml:id="A88795-004-a-0650">wee</w>
     <w join="left" lemma="will" pos="vmd" xml:id="A88795-004-a-0651">'d</w>
     <w lemma="assign" pos="vvi" xml:id="A88795-004-a-0660">assign</w>
     <w lemma="your" pos="po" xml:id="A88795-004-a-0670">your</w>
     <w lemma="guard" pos="n1" xml:id="A88795-004-a-0680">Guard</w>
     <pc xml:id="A88795-004-a-0690">,</pc>
     <w lemma="but" pos="acp" xml:id="A88795-004-a-0700">but</w>
     <w lemma="that" pos="cs" xml:id="A88795-004-a-0710">that</w>
     <w lemma="we" pos="pns" xml:id="A88795-004-a-0720">we</w>
     <w lemma="be" pos="vvb" xml:id="A88795-004-a-0730">are</w>
    </l>
    <l xml:id="A88795-e10540">
     <w lemma="assure" pos="vvn" reg="Assured" xml:id="A88795-004-a-0740">Assur'd</w>
     <pc xml:id="A88795-004-a-0750">,</pc>
     <w lemma="your" pos="po" xml:id="A88795-004-a-0760">your</w>
     <w lemma="duke" pos="n2" xml:id="A88795-004-a-0770">DUKES</w>
     <w lemma="a" pos="d" xml:id="A88795-004-a-0780">a</w>
     <w lemma="great" pos="jc" xml:id="A88795-004-a-0790">greater</w>
     <w lemma="god" pos="n1" xml:id="A88795-004-a-0800">God</w>
     <w lemma="of" pos="acp" xml:id="A88795-004-a-0810">of</w>
     <w lemma="war" pos="n1" xml:id="A88795-004-a-0820">War</w>
     <pc xml:id="A88795-004-a-0830">:</pc>
    </l>
    <l xml:id="A88795-e10550">
     <w lemma="the" pos="d" xml:id="A88795-004-a-0840">The</w>
     <w lemma="grace" pos="n2" xml:id="A88795-004-a-0850">Graces</w>
     <w lemma="to" pos="prt" xml:id="A88795-004-a-0860">to</w>
     <w lemma="attend" pos="vvi" xml:id="A88795-004-a-0870">attend</w>
     <w lemma="you" pos="pn" xml:id="A88795-004-a-0880">you</w>
     <w join="right" lemma="we" pos="pns" reg="we" xml:id="A88795-004-a-0890">wee</w>
     <w join="left" lemma="will" pos="vmd" xml:id="A88795-004-a-0891">'d</w>
     <w lemma="call" pos="vvi" xml:id="A88795-004-a-0900">call</w>
     <w lemma="forth" pos="av" xml:id="A88795-004-a-0910">forth</w>
     <pc xml:id="A88795-004-a-0920">,</pc>
    </l>
    <l xml:id="A88795-e10560">
     <w lemma="but" pos="acp" xml:id="A88795-004-a-0930">But</w>
     <w lemma="that" pos="cs" xml:id="A88795-004-a-0940">that</w>
     <w join="right" lemma="they" pos="pns" reg="they" xml:id="A88795-004-a-0950">th'</w>
     <w join="left" lemma="be" pos="vvb" xml:id="A88795-004-a-0951">are</w>
     <w lemma="all" pos="d" xml:id="A88795-004-a-0960">all</w>
     <w lemma="engross" pos="vvn" xml:id="A88795-004-a-0970">ingross'd</w>
     <w lemma="in" pos="acp" xml:id="A88795-004-a-0980">in</w>
     <w lemma="your" pos="po" xml:id="A88795-004-a-0990">your</w>
     <w lemma="own" pos="d" xml:id="A88795-004-a-1000">own</w>
     <w lemma="worth" pos="n1" xml:id="A88795-004-a-1010">worth</w>
     <pc xml:id="A88795-004-a-1020">;</pc>
    </l>
    <l xml:id="A88795-e10570">
     <w lemma="and" pos="cc" xml:id="A88795-004-a-1030">And</w>
     <w lemma="Venus" pos="nn1" rend="hi" xml:id="A88795-004-a-1040">Venus</w>
     <w lemma="with" pos="acp" xml:id="A88795-004-a-1050">with</w>
     <w lemma="her" pos="po" xml:id="A88795-004-a-1060">her</w>
     <w lemma="Cupid" pos="nn1" rend="hi" xml:id="A88795-004-a-1070">Cupid</w>
     <w lemma="too" pos="av" xml:id="A88795-004-a-1080">too</w>
     <w lemma="shall" pos="vmd" xml:id="A88795-004-a-1090">should</w>
     <w lemma="come" pos="vvi" xml:id="A88795-004-a-1100">come</w>
     <pc xml:id="A88795-004-a-1110">,</pc>
    </l>
    <l xml:id="A88795-e10600">
     <w lemma="but" pos="acp" xml:id="A88795-004-a-1120">But</w>
     <w lemma="that" pos="cs" xml:id="A88795-004-a-1130">that</w>
     <w lemma="you" pos="pn" xml:id="A88795-004-a-1140">you</w>
     <w lemma="have" pos="vvb" xml:id="A88795-004-a-1150">have</w>
     <w lemma="a" pos="d" xml:id="A88795-004-a-1160">a</w>
     <w lemma="sweet" pos="jc" xml:id="A88795-004-a-1170">sweeter</w>
     <w lemma="prince" pos="n1" xml:id="A88795-004-a-1180">Prince</w>
     <w lemma="at" pos="acp" xml:id="A88795-004-a-1190">at</w>
     <w lemma="home" pos="n1" xml:id="A88795-004-a-1200">home</w>
     <pc xml:id="A88795-004-a-1210">:</pc>
    </l>
    <l xml:id="A88795-e10610">
     <w lemma="thus" pos="av" xml:id="A88795-004-a-1220">Thus</w>
     <w lemma="poet" pos="n2" xml:id="A88795-004-a-1230">Poets</w>
     <w lemma="dream" pos="vvb" xml:id="A88795-004-a-1240">Dream</w>
     <pc xml:id="A88795-004-a-1250">,</pc>
     <w lemma="and" pos="cc" xml:id="A88795-004-a-1260">and</w>
     <w lemma="muse" pos="ng1" reg="Muse's" xml:id="A88795-004-a-1270">Muses</w>
     <w lemma="fancy" pos="n1" xml:id="A88795-004-a-1280">fancy</w>
     <w lemma="less" pos="avc-d" xml:id="A88795-004-a-1290">less</w>
    </l>
    <l xml:id="A88795-e10620">
     <w lemma="than" pos="cs" reg="Than" xml:id="A88795-004-a-1300">Then</w>
     <w lemma="what" pos="crq" xml:id="A88795-004-a-1310">what</w>
     <w lemma="fate" pos="n2" xml:id="A88795-004-a-1320">Fates</w>
     <w lemma="judge" pos="vvb" reg="judge" xml:id="A88795-004-a-1330">judg</w>
     <w lemma="you" pos="pn" xml:id="A88795-004-a-1340">you</w>
     <w lemma="worthy" pos="j" xml:id="A88795-004-a-1350">worthy</w>
     <w lemma="to" pos="prt" xml:id="A88795-004-a-1360">to</w>
     <w lemma="possess" pos="vvi" xml:id="A88795-004-a-1370">possess</w>
     <pc xml:id="A88795-004-a-1380">:</pc>
    </l>
    <l xml:id="A88795-e10630">
     <w lemma="our" pos="po" xml:id="A88795-004-a-1390">Our</w>
     <w lemma="Pegasus" pos="nn1" rend="hi" xml:id="A88795-004-a-1400">Pegasus</w>
     <w lemma="with" pos="acp" xml:id="A88795-004-a-1410">with</w>
     <w lemma="duty" pos="n1" xml:id="A88795-004-a-1420">duty</w>
     <w lemma="wing" pos="j-vn" reg="winged" xml:id="A88795-004-a-1430">wing'd</w>
     <w lemma="we" pos="pns" xml:id="A88795-004-a-1440">we</w>
     <w lemma="show" pos="vvb" xml:id="A88795-004-a-1450">show</w>
     <pc xml:id="A88795-004-a-1460">,</pc>
    </l>
    <l xml:id="A88795-e10650">
     <w lemma="other" pos="pi2-d" xml:id="A88795-004-a-1470">Others</w>
     <w lemma="may" pos="vmb" xml:id="A88795-004-a-1480">may</w>
     <w lemma="high" pos="avc-j" xml:id="A88795-004-a-1490">higher</w>
     <w lemma="fly" pos="vvi" xml:id="A88795-004-a-1500">fly</w>
     <pc xml:id="A88795-004-a-1510">,</pc>
     <w lemma="none" pos="pix" xml:id="A88795-004-a-1520">none</w>
     <w lemma="stoop" pos="vvb" xml:id="A88795-004-a-1530">stoop</w>
     <w lemma="so" pos="av" xml:id="A88795-004-a-1540">so</w>
     <w lemma="low" pos="j" xml:id="A88795-004-a-1550">low</w>
     <pc unit="sentence" xml:id="A88795-004-a-1560">.</pc>
    </l>
    <trailer xml:id="A88795-e10660">
     <w lemma="the" pos="d" xml:id="A88795-004-a-1570">The</w>
     <w lemma="end" pos="n1" xml:id="A88795-004-a-1580">END</w>
     <pc unit="sentence" xml:id="A88795-004-a-1590">.</pc>
    </trailer>
    <pb facs="tcp:154247:4" xml:id="A88795-004-b"/>
    <pb facs="tcp:154247:5" rend="simple:additions" xml:id="A88795-005-a"/>
    <pb facs="tcp:154247:5" xml:id="A88795-005-b"/>
   </div>
  </body>
  <back xml:id="A88795-e10010-b"/>
 </text>
</TEI>
